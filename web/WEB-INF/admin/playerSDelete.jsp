<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : playerSDelete
    Created on : 07-Apr-2013, 17:43:47
    Author     : franks
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/clubModPage.css">
        <title>ClubDeletePage</title>
    </head>
    <body id="anyBody" >
 
 <div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		<div id="nav">
                    
                        	
		</div>
		
		<div id="content">	
		
					
		  
                     <div id="showContent">	
			<div id="update">
                              <%
        
                              String member = request.getParameter("member");                      
                              
                              %>
                             
                              <sql:update var="result" dataSource="jdbc/project">
                                  DELETE FROM playerstats
                                  WHERE MemberID = <%=member%>
                              </sql:update>
                                 
                              <form name="clubDelete"  method="get" action= "<c:url value='playerS'/>">  
                                  
                                  <table>
                                      <tr>
                                          <th>Record Removed Successfully</th>
                                      </tr>
                                    <td>
			              <div class="tableRow">                       
									
                                      <p><input type="submit" name="return" value="Return"></p>				
								
			              </div>
                                    </td>                                  
                                  </table>                                	
                              </form>                       
                         </div>	                    
                      </div>  		 
				
					 
			     				   
			
                    
                      </div>  
								   
	         </div>  
					
			<div id="clear">	

			</div>
					
                       <div id="footer">	
					   <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
					  </div>
		       </div>

	</div>
 
 </body>
</html>
