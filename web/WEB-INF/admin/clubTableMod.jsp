<%-- 
    Document   : clubTableMod
    Created on : 05-Apr-2013, 09:02:29
    Author     : franks
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<!DOCTYPE html>
<html>

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/clubModPage.css">
        <title></title>

</head>
 
 <body id="anyBody" >
 
 <div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		<div id="nav">
                  
                        	
		</div>
		
		<div id="content">	
		
					
		  
                     <div id="showContent">	                     
	             <div id ="regDivOne">
                    <form name="clubDelete" method="get" action= "<c:url value='clubTableDelete'/>">                           		   
                    <table font face="verdana">  
                        <tr>
                            
                            <th>C l u b   T a b l e</th>
                            
                        </tr>                        
                        
                        <tr>
                          <td>
			     <div class="tableRow">									
                               <p>ClubID:</p>
                               <p><input type="text" name="clubid" value="" align="left"></p>				
					
			     </div>
                          </td>
                           <td>
			    <div class="tableRow">                     
                             <p><input type="submit" name="submit" value="Delete Exsiting Table"></p>								
								
			    </div>                           
                            
                        </td>
                        </tr>                  
                   </table>					   
	       </form>	 
             </div>                                 
                         
                 <div id ="regDivTwo">      
                    <form name="clubEdit"  method="get" action="<c:url value='clubTableUpdate'/>">                           		   
                    <table>        
                          <tr>
                          <td>
			  <div class="tableRow">									
                           <p>ClubID   </p>
									
                           <input type="text"  name="clubid" value="" align="left">					
                          
		    	  </div>
                           </td>
                          <td>
			  <div class="tableRow">                       
									
                              <p><input type="submit" name="insert" value="Update Existing Table"></p>				
								
			   </div>
                          </td>
                      </tr>
                    
                        <tr>
                            <td>				
			    <div class="tableRow">							
				
                            <p>PhoneNo</p>
									
                            <p>	<input type="text" name="phoneNo" value=""></p>
			     </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>				
			    <div class="tableRow">							
				
                            <p>Address</p>
									
                            <p>	<input type="text" name="address" value="" align="right"></p>
			     </div>
                            </t
                        </tr>
                        <tr>
                            <td>				
			    <div class="tableRow">							
				
                            <p>Name</p>
									
                            <p>	<input type="text"  name="name" value="" align="right"></p>
			     </div>
                            </td>
                        </tr>	  
			                       
                 </table>					   
	       </form>	   	 
             </div>               
                </div> 
	         </div>  
					
			<div id="clear">	

			</div>
					
                       <div id="footer">	
					   <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
					  </div>
		       </div>

	</div>
 
 </body>
 
 
 </html> 
