<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : clubTableUpdate
    Created on : 06-Apr-2013, 13:20:43
    Author     : franks
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/clubModPage.css">
        <title>ClubUpdatePage</title>
    </head>
    <body id="anyBody" >
 
 <div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		
		
		<div id="content">	
		
					
		  
                     <div id="showContent">	
					 
                         <div id="update">
                              <%
        
                              String clubid = request.getParameter("clubid");
                              
                              String phoneNo = request.getParameter("phoneNo");
                              
                              String address = request.getParameter("address");
                              
                              String name = request.getParameter("name");
                              
                              %>
                              
                             <sql:update var="result" dataSource="jdbc/project">
                                 UPDATE club
                                 SET PhoneNo = <%=phoneNo%>,Address = '<%=address%>',Name = '<%=name%>'
                                 WHERE ClubID = <%=clubid%>
                             </sql:update>
                                 
                              <form name="clubUpdate"  method="get" action= "clubTableMod.jsp">  
                                  
                                  <table>
                                      <tr>
                                          <th>Table Updated Successfully</th>
                                      </tr>
                                    <td>
			              <div class="tableRow">                       
									
                                      <p><input type="submit" name="return" value="Return"></p>				
								
			              </div>
                                    </td>                                  
                                  </table>                                	
                              </form>                       
                         </div>	                    
                      </div>  
								   
	         </div>  
					
			<div id="clear">	

			</div>
					
                       <div id="footer">	
					   <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
					  </div>
		       </div>

	</div>
 
 </body>
</html>
