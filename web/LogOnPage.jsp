<%-- 
    Document   : LogOnPage
    Created on : Feb 17, 2013, 6:06:08 PM
    Author     : franks
--%>

<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <title>JSP Page</title>
        <script type="text/javascript">
            function checkform(){
               
               var filter =  /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
               
                if (document.logOn.username.value =="") {
                                                       // something is wrong
                   alert('Please Enter Your Name');
                   
                   document.logOn.username.focus();    // gives the username box focus
                   
                   return false;
                   
               }else if (filter.search(document.logOn.email.value)==-1)  {
                         alert('Please Enter a valid Email Address')
                         document.logOn.email.focus();                                      // either left blank or not a valid email address
                  
                }else if(document.logOn.email.value ==""){
                                                              
                        alert('Please Enter Email')
                    document.logOn.email.focus();            // gives the email box focus
                    return false;  
               }//end else if statement             
                  
            }//end checkform function           
            
        </script>
      
    </head>
 
 <body id="anyBody" >
 
	<div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		<div id="nav">
                   
		
		<div id="content">	
		
					
		  
                     <div id="showContent">
                         
                         <div id="logForm">  
                           
			<form name="logOn" method="post"  onSubmit="return checkform(this)"   action= "<c:url value='logOnPage'/>">	   
                         <table>   
                        <tr>
                          <td>
			<div class="tableRow">
									
                         <p>Name:</p>
                          <p> <input type="text"  name="username" value=""  ></p>
									
								
			</div>
                        </td>
                     </tr>
                  <tr>
                       <td>
			<div class="tableRow">
									
                         <p> Email:</p>
                         <p><input type="text" name="email" value=""></p>								
								
		          </div>    
                      </td>
                  </tr>
                   <tr>
                      <td>
                         <div class="tableRow">
									
                         <p> Submit:</p>
                         <p><input type="submit" name="submit" value="Submit"></p>								
                        					
		       </div>  
                            
                      </td>
                   </tr>
                             
               </table>
					 
                </form>		 
                         </div>     			
                        	   
			
                    
                      </div>  
								   
	         </div>  
					
			<div id="clear">	

			</div>
					
                       <div id="footer">	
					   <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
					  </div>
		       </div>

	</div>



 
 </body>
 
 
 
 
 
 </html> 