package entity;

import entity.Drilltype;
import entity.Session;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Drill.class)
public class Drill_ { 

    public static volatile CollectionAttribute<Drill, Drilltype> drilltypeCollection;
    public static volatile SingularAttribute<Drill, String> drillID;
    public static volatile SingularAttribute<Drill, Session> sessionSessionID;

}