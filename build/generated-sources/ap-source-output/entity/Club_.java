package entity;

import entity.Member1;
import entity.Team;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Club.class)
public class Club_ { 

    public static volatile SingularAttribute<Club, Integer> phoneNo;
    public static volatile CollectionAttribute<Club, Member1> member1Collection;
    public static volatile SingularAttribute<Club, String> clubID;
    public static volatile SingularAttribute<Club, String> address;
    public static volatile SingularAttribute<Club, String> name;
    public static volatile CollectionAttribute<Club, Team> teamCollection;

}