package entity;

import entity.Club;
import entity.Goals;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Member1.class)
public class Member1_ { 

    public static volatile SingularAttribute<Member1, Integer> phoneNo;
    public static volatile SingularAttribute<Member1, String> email;
    public static volatile SingularAttribute<Member1, String> name;
    public static volatile SingularAttribute<Member1, Club> clubClubID;
    public static volatile SingularAttribute<Member1, String> userName;
    public static volatile SingularAttribute<Member1, String> image;
    public static volatile SingularAttribute<Member1, String> memberID;
    public static volatile CollectionAttribute<Member1, Goals> goalsCollection;
    public static volatile SingularAttribute<Member1, String> password;

}