package entity;

import entity.Drill;
import entity.DrilltypePK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Drilltype.class)
public class Drilltype_ { 

    public static volatile SingularAttribute<Drilltype, DrilltypePK> drilltypePK;
    public static volatile SingularAttribute<Drilltype, String> name;
    public static volatile SingularAttribute<Drilltype, Drill> drill;

}