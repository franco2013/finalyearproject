package entity;

import entity.Drill;
import entity.Sessiontype;
import entity.Team;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Session.class)
public class Session_ { 

    public static volatile SingularAttribute<Session, Date> startTime;
    public static volatile SingularAttribute<Session, Integer> sessionID;
    public static volatile CollectionAttribute<Session, Sessiontype> sessiontypeCollection;
    public static volatile SingularAttribute<Session, Date> date;
    public static volatile SingularAttribute<Session, Date> endTime;
    public static volatile SingularAttribute<Session, Team> teamTeamID;
    public static volatile CollectionAttribute<Session, Drill> drillCollection;

}