package entity;

import entity.Session;
import entity.SessiontypePK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Sessiontype.class)
public class Sessiontype_ { 

    public static volatile SingularAttribute<Sessiontype, SessiontypePK> sessiontypePK;
    public static volatile SingularAttribute<Sessiontype, Session> session;
    public static volatile SingularAttribute<Sessiontype, String> description;

}