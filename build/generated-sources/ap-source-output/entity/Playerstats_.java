package entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Playerstats.class)
public class Playerstats_ { 

    public static volatile SingularAttribute<Playerstats, Integer> phoneNo;
    public static volatile SingularAttribute<Playerstats, String> position;
    public static volatile SingularAttribute<Playerstats, String> startSeason;
    public static volatile SingularAttribute<Playerstats, String> goalsScored;
    public static volatile SingularAttribute<Playerstats, String> height;
    public static volatile SingularAttribute<Playerstats, String> foot;
    public static volatile SingularAttribute<Playerstats, String> name;
    public static volatile SingularAttribute<Playerstats, String> memberID;

}