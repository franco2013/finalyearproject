package entity;

import entity.Club;
import entity.Session;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2013-04-06T11:24:39")
@StaticMetamodel(Team.class)
public class Team_ { 

    public static volatile SingularAttribute<Team, Integer> clubClubID;
    public static volatile CollectionAttribute<Team, Session> sessionCollection;
    public static volatile SingularAttribute<Team, Integer> teamID;
    public static volatile SingularAttribute<Team, String> estDate;
    public static volatile SingularAttribute<Team, String> leagueLeagueID;
    public static volatile SingularAttribute<Team, Club> clubClubID1;

}