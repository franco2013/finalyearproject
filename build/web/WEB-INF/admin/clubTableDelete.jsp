<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : clubTableDelete
    Created on : 06-Apr-2013, 14:14:32
    Author     : franks
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/clubModPage.css">
        <title>ClubDeletePage</title>
    </head>
    <body id="anyBody" >
 
 <div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		<div id="nav">
                    
                        	
		</div>
		
		<div id="content">	
		
					
		  
                     <div id="showContent">	
			<div id="update">
                             <%
        
                              String clubid = request.getParameter("clubid");                      
                              
                              %>
                              <sql:update var="result" dataSource="jdbc/project">
                                  DELETE FROM club
                                  WHERE ClubID = <%=clubid%>
                              </sql:update>
                             
                                
                              <form name="clubDelete"  method="get" action="<c:url value='clubTableMod'/>">  
                                  
                                  <table>
                                      <tr>
                                          <th>Record Removed Successfully</th>
                                      </tr>
                                    <td>
			              <div class="tableRow">                       
									
                                      <p><input type="submit" name="return" value="Return"></p>				
								
			              </div>
                                    </td>                                  
                                  </table>                                	
                              </form>                       
                         </div>	                    
                      </div>  		 
				
					 
			     				   
			
                    
                      </div>  
								   
	         </div>  
					
			<div id="clear">	

			</div>
					
                       <div id="footer">	
					   <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
					  </div>
		       </div>

	</div>
 
 </body>
</html>
