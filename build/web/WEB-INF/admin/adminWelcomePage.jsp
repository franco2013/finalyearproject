<%-- 
    Document   : adminWelcomePage
    Created on : 03-Apr-2013, 15:23:48
    Author     : franks
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css"  href="css/adminWelcomPage.css">
        <title></title>

</head>
 
 <body id="anyBody" >
 
      <div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		<div id="nav">
                    
                        	
		</div>
		
		<div id="content">	
					
		  
                <div id="showContent">
                   
                    <table border="1"  style="font-family:verdana">
                            <caption>Choose Table To Modify</caption>
                            
                           
                                        
                     
                            <tr bgcolor="#d80000">
                                <td><a href="<c:url value='clubTableMod'/>">Club Table</a> </td>
                                <td></td>
                                <td><a href="<c:url value='playerS'/>">Player Stats Table</a></td>
                            </tr>
                       
                    </table>
               
                    
                                      
                </div>  
								   
	         </div>  
					
			<div id="clear">	

	        </div>
					
                       <div id="footer">	
		         <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
		         </div>
		       </div>
        </div> 
    </body>
 
 </html> 