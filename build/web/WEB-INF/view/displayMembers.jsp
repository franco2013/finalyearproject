<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%-- 
    Document   : displayMembers
    Created on : 05-Mar-2013, 16:59:09
    Author     : franks
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/displayMembersTable.css">
        <title>JSP Page</title>
    </head>
 
 <body id="anyBody" >

	<div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="995" height="135">		
			
		</div>

		<div id="nav">
                    <div id="navNav">                 
                         <ul>
                             <li><a href="<c:url value='theLogOnPage'/>">Log On</a></li>
                              <li><a href="<c:url value='theRegClubDetailsPage'/>">Register</a></li>
                             
                             <li><a href="<c:url value='thePlayerStatsPage'/>">View Players</a></li>
                             <li><a href="<c:url value='theIndexPage'/>">Home</a></li>
                         </ul> 
                    </div>   
                        	
		</div>
		
		<div id="content">	
		<div id="sideNav">
                    <div id="navDiv">
                        <ul class="sidemenu">
                            
                               <li><a href="<c:url value='theLogOnPage'/>">Log On</a></li>
                              <li><a href="<c:url value='theRegClubDetailsPage'/>">Register</a></li>
                             
                             <li><a href="<c:url value='thePlayerStatsPage'/>">View Players</a></li>
                             <li><a href="<c:url value='theIndexPage'/>">Home</a></li>
                        </ul>
                    </div>
                    
                     
		</div>
					
		                                     
                     <div id="showContent">	
                         <div id="members">		 
			<sql:query var="memResult" dataSource="jdbc/project">
                                       SELECT* FROM member
                         </sql:query>
                                   
                                       <table border="1">
                                       
                                       <!-- column headers -->
                                       <tr>                                           <!-- looping thru the columns -->
                                       <c:forEach var="columnName" items="${memResult.columnNames}">     
                                           <th><c:out value="${columnName}"/></th>
                                       </c:forEach>
                                       </tr>
                                       <!-- column data -->                                       <!-- looping thru the rows -->
                                       <c:forEach var="row" items="${memResult.rowsByIndex}">
                                           <tr>
                                           <c:forEach var="column" items="${row}">                 <!-- looping across the row -->
                                               <td><c:out value="${column}"/></td>
                                           </c:forEach>
                                           </tr>
                                       </c:forEach>
                                   </table>

                     </div>
                    
                      </div>  
								   
	         </div>  
					
			<div id="clear">	

			</div>
					
                       <div id="footer">	
					   <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
					  </div>
		       </div>

	</div>



 
 </body>
 
 
 
 
 </html> 