<%-- 
    Document   : RegitrationPage
    Created on : Feb 17, 2013, 5:49:40 PM
    Author     : franks
--%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/RegPesonnelDetails.css">
        <title>JSP Page</title>
   </head>
 
 <body id="anyBody">
<%
     
   String ClubID = (String)request.getAttribute("clubID");
      // String ClubID = "34";                                       
%>
	<div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		<div id="nav">
                    <div id="navNav">                 
                         <ul>
                             <li><a href="<c:url value='theLogOnPage'/>">Log On</a></li>
                              <li><a href="<c:url value='theRegClubDetailsPage'/>">Register</a></li>
                             
                             <li><a href="<c:url value='thePlayerStatsPage'/>">View Players</a></li>
                             <li><a href="<c:url value='theIndexPage'/>">Home</a></li>
                         </ul> 
                    </div>   
                        	
		</div>
		
		<div id="content">	
		<div id="sideNav">
                    <div id="navDiv">
                        <ul class="sidemenu">
                            
                              <li><a href="<c:url value='theLogOnPage'/>">Log On</a></li>
                              <li><a href="<c:url value='theRegClubDetailsPage'/>">Register</a></li>
                             
                             <li><a href="<c:url value='thePlayerStatsPage'/>">View Players</a></li>
                             <li><a href="<c:url value='theIndexPage'/>">Home</a></li>
                        </ul>
                    </div>
                    
                     
		</div>
					
		  
                     <div id="showContent">
                         
	 <div id="regformDiv">							   
     		<form name="regpage" method="GET" action="<c:url value='regPagePerson'/>" >		   
                    <table>   
                        <tr>
                            
                            <td>	    					
				<input type="hidden" name="clubID" value=<%= ClubID%>>
                            </td>                          
                        </tr>  
                        <tr>   
                            <td>
			<div class="tableRow">
									
                         <p>Name:</p>
                          <p> <input type="text" name="name" value=""></p>									
								
			</div>
                        </td>
                        <td>
			<div class="tableRow">
									
                         <p> UserName:</p>
                         <p><input type="text" name="username" value=""></p>								
								
			</div>                  
                            
                        </td>
                        </tr>
                      <tr>
                          
                           <td>
			  <div class="tableRow">
									
                          <p> Phone No: </p>
									
                           <p><input type="text" name="phoneNo" value=""></p>				
								
			   </div>
                          </td>          
                          
                          
                          <td>
			  <div class="tableRow">
									
                           <p>Password</p>
									
                           <p><input type="password" name="password" value="">	</p>					
                          
		    	   </div>
                          </td>
                         
                    </tr>
                    <tr>
                          <td>
			   <div class="tableRow">
									
                           <p> Email: </p>
									
                           <p>	<input type="text" name="email" value=""> </p>					
								
	  		    </div>
                          </td>
                          <td>                             
                          
	   		   <div class="tableRow">
									
                           <p> Address:</p>									
									
                           <p>	<input type="text" name="address" value=""></p>
			
			   </div>                             
                              									
                           </td>
                        </tr>
                        <tr>                            
                            
                            <td>
                            <p>	<input type="submit" value="Submit"></p>
			        
                            </td>
                        </tr>
		          </table>					   
	                </form>	   
                    </div>
                      </div>  
								   
	         </div>  
					
			<div id="clear">	

			</div>
					
                       <div id="footer">	
					   <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
					  </div>
		       </div>

	</div>



 
 </body>
 
 
 
 
 </html> 