<%-- 
    Document   : displayPlayerStats
    Created on : 02-Mar-2013, 17:40:00
    Author     : franks
--%>

<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/displayPlayerStats.css">
        <title>JSP Page</title>
    </head>
    <body>     
	
		
	<body id="anyBody">

			<div id="pagewidth">

					<div id="header">		
					  <img src="images/headerTwo.jpg" width="865" height="135">
					</div>

					<div id="nav">
                                           
                                           <div id="navNav">                 
                         <ul>
                             <li><a href="<c:url value='theLogOnPage'/>">Log On</a></li>
                              <li><a href="<c:url value='theRegClubDetailsPage'/>">Register</a></li>
                             
                             <li><a href="<c:url value='thePlayerStatsPage'/>">View Players</a></li>
                             <li><a href="<c:url value='theIndexPage'/>">Home</a></li>
                         </ul> 
                    </div>   
                                          			
					</div>
		<div id="sideNav">
		 <div id="navDiv">
                        <ul class="sidemenu">
                            
                              <li><a href="<c:url value='theLogOnPage'/>">Log On</a></li>
                              <li><a href="<c:url value='theRegClubDetailsPage'/>">Register</a></li>
                             
                             <li><a href="<c:url value='thePlayerStatsPage'/>">View Players</a></li>
                             <li><a href="<c:url value='theIndexPage'/>">Home</a></li>
                        </ul>
                    </div>
		</div>
					
			<div id="content">	
                            <div id="query">
                                   <sql:query var="anyResult" dataSource="jdbc/project">
                                       SELECT * FROM playerstats
                                   </sql:query>
                                   
                                   <table border="1"  bgcolor="#d4d6d3">
                                       <!-- column headers -->
                                       <tr>
                                           <c:forEach var="columnName" items="${anyResult.columnNames}">
                                               <th><c:out value="${columnName}"/></th>
                                           </c:forEach>
                                       </tr>
                                       <!-- column data -->
                                       <c:forEach var="row" items="${anyResult.rowsByIndex}">
                                           <tr>
                                               <c:forEach var="column" items="${row}">
                                                   <td><c:out value="${column}"/></td>
                                               </c:forEach>
                                           </tr>
                                       </c:forEach>
                                   </table>		   
						   
                             </div>				   
								   
			   </div>
					
					<div id="clear">	

					</div>
					
                       <div id="footer">	
                                 
					   </div>

			</div>


	</body>
   
</html>
