<%-- 
    Document   : adminFailurePage
    Created on : 04-Apr-2013, 15:29:27
    Author     : franks
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<!DOCTYPE html>
<html>

<head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" type="text/css" href="css/adminFailurePage.css">
        <title></title>

</head>
 
 <body id="anyBody" >
 
      <div id="pagewidth">

		<div id="header">

                        <img src="images/headerTwo.jpg" width="865" height="135">		
			
		</div>

		<div id="nav">
                    
                        	
		</div>
		
		<div id="content">	
					
		  
                <div id="showContent">	     
                    <table border="1"  style="font-family:verdana">
                            <caption>Return To Log On Page</caption>
                           
                                              
                            <tr bgcolor="#d80000">
                                <td> </td>
                               <td><a href="<c:url value='theLogOnPage'/>">Log On Page</a> </td>  
                                <td></td>
                            </tr>
                       
                    </table>
               
                  </div>  
								   
	         </div>  
					
			<div id="clear">	

	        </div>
					
                       <div id="footer">	
		         <div id="ftext">
                               <p>&copycopyright | privacy policy | terms of service </p>
		         </div>
		       </div>
        </div> 
    </body>
 
 </html> 