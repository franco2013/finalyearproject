package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class adminLoginPage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_url_value_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_url_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_url_value_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write('`');
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/adminLoginPage.css\">\n");
      out.write("        <title></title>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write(" \n");
      out.write(" <body id=\"anyBody\" >\n");
      out.write(" \n");
      out.write("      <div id=\"pagewidth\">\n");
      out.write("\n");
      out.write("\t\t<div id=\"header\">\n");
      out.write("\n");
      out.write("                        <img src=\"images/headerTwo.jpg\" width=\"865\" height=\"135\">\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t</div>\n");
      out.write("\n");
      out.write("\t\t<div id=\"nav\">\n");
      out.write("                    \n");
      out.write("                        \t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\t\t<div id=\"content\">\t\n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t  \n");
      out.write("                <div id=\"showContent\">\t     \n");
      out.write("                     <div id=\"admin\">\t\t \n");
      out.write("                         <form name=\"adminlogOn\" method=\"post\"    action=\"");
      if (_jspx_meth_c_url_0(_jspx_page_context))
        return;
      out.write("\" >\n");
      out.write("                            \n");
      out.write("                         <table>   \n");
      out.write("                        <tr>\n");
      out.write("                          <td>\n");
      out.write("\t\t\t<div class=\"tableRow\">\t\t\t\t\t\t\t\t\t\n");
      out.write("                         <p>AdminName:</p>\n");
      out.write("                          <p> <input type=\"text\"  name=\"adminName\" value=\"\"  ></p>\t\t\n");
      out.write("\t\t\t</div>\n");
      out.write("                          </td>\n");
      out.write("                        </tr>\n");
      out.write("                      <tr>\n");
      out.write("                       <td>\n");
      out.write("\t\t\t<div class=\"tableRow\">\t\t\t\t\t\t\t\t\t\n");
      out.write("                         <p> Password:</p>\n");
      out.write("                         <p><input type=\"password\" name=\"password\" value=\"\" ></p>\t\t\t\t\t\t\t\t\n");
      out.write("\t\t          </div>    \n");
      out.write("                      </td>\n");
      out.write("                  </tr>\n");
      out.write("                   <tr>\n");
      out.write("                      <td>\n");
      out.write("                         <div class=\"tableRow\">\t\t\t\t\t\t\t\t\t\n");
      out.write("                         <p> Submit:</p>\n");
      out.write("                         <p><input type=\"submit\" name=\"submit\" value=\"Submit\" align=\"right\"></p>                       \t\t\t\t\t\n");
      out.write("\t\t       </div>                             \n");
      out.write("                      </td>\n");
      out.write("                   </tr>                             \n");
      out.write("                   </table>\t\t\t\t\t \n");
      out.write("                </form>\t\t \n");
      out.write("       </div>                   \n");
      out.write("                      </div>  \n");
      out.write("\t\t\t\t\t\t\t\t   \n");
      out.write("\t         </div>  \n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t<div id=\"clear\">\t\n");
      out.write("\n");
      out.write("\t        </div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("                       <div id=\"footer\">\t\n");
      out.write("\t\t         <div id=\"ftext\">\n");
      out.write("                               <p>&copycopyright | privacy policy | terms of service </p>\n");
      out.write("\t\t         </div>\n");
      out.write("\t\t       </div>\n");
      out.write("        </div> \n");
      out.write("    </body>\n");
      out.write(" \n");
      out.write(" </html> ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_url_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_0 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_0.setPageContext(_jspx_page_context);
    _jspx_th_c_url_0.setParent(null);
    _jspx_th_c_url_0.setValue("admin");
    int _jspx_eval_c_url_0 = _jspx_th_c_url_0.doStartTag();
    if (_jspx_th_c_url_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
    return false;
  }
}
