package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class welcomePage_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.apache.jasper.runtime.TagHandlerPool _jspx_tagPool_c_url_value_nobody;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _jspx_tagPool_c_url_value_nobody = org.apache.jasper.runtime.TagHandlerPool.getTagHandlerPool(getServletConfig());
  }

  public void _jspDestroy() {
    _jspx_tagPool_c_url_value_nobody.release();
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/wel.css\">\n");
      out.write("        <title></title>\n");
      out.write("        \n");
     
   String username = (String)request.getAttribute("username");
   String email = (String)request.getAttribute("email"); 
                                        

      out.write("\n");
      out.write("\n");
      out.write("</head>\n");
      out.write(" \n");
      out.write(" <body id=\"anyBody\" >\n");
      out.write("\n");
      out.write("\t<div id=\"pagewidth\">\n");
      out.write("\n");
      out.write("\t\t<div id=\"header\">\n");
      out.write("\n");
      out.write("                        <img src=\"images/headerTwo.jpg\" width=\"865\" height=\"135\">\t\t\t\t\n");
      out.write("\t\t</div>\n");
      out.write("\n");
      out.write("\t\t<div id=\"nav\">\n");
      out.write("                    <div id=\"navNav\">                 \n");
      out.write("                         <ul>\n");
      out.write("                            <li><a href=\"/WEB-INF/LogOnPage.jsp\">Log On</a></li>\n");
      out.write("                             <li><a href=\"/WEB-INF/view/RegClubDetails.jsp\">Register</a></li>\n");
      out.write("                             \n");
      out.write("                             <li><a href=\"/WEB-INF/view/displayPlayerStats.jsp\">View Players</a></li>\n");
      out.write("                             <li><a href=\"/WEB-INF/index.jsp\">Home</a></li>\n");
      out.write("                         </ul> \n");
      out.write("                    </div>   \n");
      out.write("                        \t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\t\t<div id=\"content\">\t\n");
      out.write("\t\t<div id=\"sideNav\">\n");
      out.write("                    <div id=\"navDiv\">\n");
      out.write("                        <ul class=\"sidemenu\">\n");
      out.write("                            \n");
      out.write("                              <li><a href=\"");
      if (_jspx_meth_c_url_0(_jspx_page_context))
        return;
      out.write("\">Log On</a></li>\n");
      out.write("                              <li><a href=\"");
      if (_jspx_meth_c_url_1(_jspx_page_context))
        return;
      out.write("\">Register</a></li>\n");
      out.write("                             \n");
      out.write("                             <li><a href=\"");
      if (_jspx_meth_c_url_2(_jspx_page_context))
        return;
      out.write("\">View Players</a></li>\n");
      out.write("                             <li><a href=\"");
      if (_jspx_meth_c_url_3(_jspx_page_context))
        return;
      out.write("\">Home</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>                    \n");
      out.write("                     \n");
      out.write("\t\t</div>\t\t\t\t\t\n");
      out.write("\t\t  \n");
      out.write("                     <div id=\"showContent\">\n");
      out.write("                         \n");
      out.write("                              <div id=\"logForm\">  \n");
      out.write("                           \n");
      out.write("\t\t\t\t   \n");
      out.write("                  <table  width=\"350\">   \n");
      out.write("                       \n");
      out.write("                  <tr>\n");
      out.write("                       <td>\n");
      out.write("\t\t\t\n");
      out.write("\t\t\t <p> Hello and Welcome. Your UserName Is ");
      out.print(username);
      out.write("</p> \n");
      out.write("                         <p> And Your Email Address is ");
      out.print(email);
      out.write("</p> \t\t\t\t\t\t\n");
      out.write("                       \n");
      out.write("                       \t\t\t\t\t\t\t\t\n");
      out.write("\t\t  \n");
      out.write("                      </td>\n");
      out.write("                  </tr>\n");
      out.write("                  \n");
      out.write("                    <tr>                  \n");
      out.write("\t\t\t\n");
      out.write("\t\t\t  \n");
      out.write("                         <td><a href=\"");
      if (_jspx_meth_c_url_4(_jspx_page_context))
        return;
      out.write("\">Admin Page</a> </td>            \n");
      out.write("                       \t\t\t\t\t\t\t\t\n");
      out.write("\t\t  \n");
      out.write("                     \n");
      out.write("                  </tr>\n");
      out.write("                   \n");
      out.write("                             \n");
      out.write("               </table>\n");
      out.write("\t\t\t\t\t \n");
      out.write("              \t \n");
      out.write("                         </div>     \t\n");
      out.write("                      \n");
      out.write("\t\t\t\t                  \n");
      out.write("                      </div>  \n");
      out.write("\t\t\t\t\t\t\t\t   \n");
      out.write("\t         </div>  \n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t<div id=\"clear\">\t\n");
      out.write("\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("                       <div id=\"footer\">\t\n");
      out.write("\t\t\t\t\t   <div id=\"ftext\">\n");
      out.write("                               <p>&copycopyright | privacy policy | terms of service </p>\n");
      out.write("\t\t\t\t\t  </div>\n");
      out.write("\t\t       </div>\n");
      out.write("\n");
      out.write("\t</div>\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write(" \n");
      out.write(" </body>\n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" \n");
      out.write(" </html> ");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }

  private boolean _jspx_meth_c_url_0(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_0 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_0.setPageContext(_jspx_page_context);
    _jspx_th_c_url_0.setParent(null);
    _jspx_th_c_url_0.setValue("theLogOnPage");
    int _jspx_eval_c_url_0 = _jspx_th_c_url_0.doStartTag();
    if (_jspx_th_c_url_0.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_0);
    return false;
  }

  private boolean _jspx_meth_c_url_1(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_1 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_1.setPageContext(_jspx_page_context);
    _jspx_th_c_url_1.setParent(null);
    _jspx_th_c_url_1.setValue("theRegClubDetailsPage");
    int _jspx_eval_c_url_1 = _jspx_th_c_url_1.doStartTag();
    if (_jspx_th_c_url_1.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_1);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_1);
    return false;
  }

  private boolean _jspx_meth_c_url_2(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_2 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_2.setPageContext(_jspx_page_context);
    _jspx_th_c_url_2.setParent(null);
    _jspx_th_c_url_2.setValue("thePlayerStatsPage");
    int _jspx_eval_c_url_2 = _jspx_th_c_url_2.doStartTag();
    if (_jspx_th_c_url_2.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_2);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_2);
    return false;
  }

  private boolean _jspx_meth_c_url_3(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_3 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_3.setPageContext(_jspx_page_context);
    _jspx_th_c_url_3.setParent(null);
    _jspx_th_c_url_3.setValue("theIndexPage");
    int _jspx_eval_c_url_3 = _jspx_th_c_url_3.doStartTag();
    if (_jspx_th_c_url_3.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_3);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_3);
    return false;
  }

  private boolean _jspx_meth_c_url_4(PageContext _jspx_page_context)
          throws Throwable {
    PageContext pageContext = _jspx_page_context;
    JspWriter out = _jspx_page_context.getOut();
    //  c:url
    org.apache.taglibs.standard.tag.rt.core.UrlTag _jspx_th_c_url_4 = (org.apache.taglibs.standard.tag.rt.core.UrlTag) _jspx_tagPool_c_url_value_nobody.get(org.apache.taglibs.standard.tag.rt.core.UrlTag.class);
    _jspx_th_c_url_4.setPageContext(_jspx_page_context);
    _jspx_th_c_url_4.setParent(null);
    _jspx_th_c_url_4.setValue("adminLogOnPage");
    int _jspx_eval_c_url_4 = _jspx_th_c_url_4.doStartTag();
    if (_jspx_th_c_url_4.doEndTag() == javax.servlet.jsp.tagext.Tag.SKIP_PAGE) {
      _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_4);
      return true;
    }
    _jspx_tagPool_c_url_value_nobody.reuse(_jspx_th_c_url_4);
    return false;
  }
}
