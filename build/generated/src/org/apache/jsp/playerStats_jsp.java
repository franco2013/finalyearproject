package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class playerStats_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.Vector _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public Object getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("\n");
      out.write("<head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <link rel=\"stylesheet\" type=\"text/css\" href=\"css/playerStats.css\">\n");
      out.write("        <title></title>\n");
      out.write("\n");
      out.write("</head>\n");
      out.write(" \n");
      out.write(" <body id=\"anyBody\" >\n");
      out.write(" \n");
      out.write(" <div id=\"pagewidth\">\n");
      out.write("\n");
      out.write("\t\t<div id=\"header\">\n");
      out.write("\n");
      out.write("                        <img src=\"images/headerTwo.jpg\" width=\"865\" height=\"135\">\t\t\n");
      out.write("\t\t\t\n");
      out.write("\t\t</div>\n");
      out.write("\n");
      out.write("\t\t<div id=\"nav\">\n");
      out.write("                    <div id=\"navNav\">                 \n");
      out.write("                         <ul>\n");
      out.write("                            <li><a href=\"LogOnPage.jsp\">Log On</a></li>\n");
      out.write("                             <li><a href=\"RegPage.jsp\">Register</a></li>\n");
      out.write("                             \n");
      out.write("                             <li><a href=\"displayPlayerStats.jsp\">View Players</a></li>\n");
      out.write("                             <li><a href=\"displayMembers.jsp\"> Members</a></li>\n");
      out.write("                         </ul> \n");
      out.write("                    </div>   \n");
      out.write("                        \t\n");
      out.write("\t\t</div>\n");
      out.write("\t\t\n");
      out.write("\t\t<div id=\"content\">\t\n");
      out.write("\t\t<div id=\"sideNav\">\n");
      out.write("                    <div id=\"navDiv\">\n");
      out.write("                        <ul class=\"sidemenu\">\n");
      out.write("                            \n");
      out.write("                                <li><a href=\"index.html\">Home</a></li>\n");
      out.write("                                <li><a href=\"RegPage.jsp\">Register</a></li>\n");
      out.write("                                \n");
      out.write("                                <li><a href=\"displayPlayerStats.jsp\">View Players</a></li>\n");
      out.write("                                <li><a href=\"contact.jsp\">Contact Us</a></li>\n");
      out.write("                        </ul>\n");
      out.write("                    </div>\n");
      out.write("                    \n");
      out.write("                     \n");
      out.write("\t\t</div>\t\t\t\t\t\n");
      out.write("\t\t  \n");
      out.write("                     <div id=\"showContent\">\t\n");
      out.write("\t\t\t\t\t \n");
      out.write("\t\t\t\t\n");
      out.write("\t\t\t\t\t \n");
      out.write("\t\t\t     \t\t\t   \n");
      out.write("\t\t\t\n");
      out.write("                    \n");
      out.write("                      </div>  \n");
      out.write("\t\t\t\t\t\t\t\t   \n");
      out.write("\t         </div>  \n");
      out.write("\t\t\t\t\t\n");
      out.write("\t\t\t<div id=\"clear\">\t\n");
      out.write("\n");
      out.write("\t\t\t</div>\n");
      out.write("\t\t\t\t\t\n");
      out.write("                       <div id=\"footer\">\t\n");
      out.write("\t\t\t\t\t   <div id=\"ftext\">\n");
      out.write("                               <p>&copycopyright | privacy policy | terms of service </p>\n");
      out.write("\t\t\t\t\t  </div>\n");
      out.write("\t\t       </div>\n");
      out.write("\n");
      out.write("\t</div>\n");
      out.write(" \n");
      out.write(" </body>\n");
      out.write(" \n");
      out.write(" \n");
      out.write(" </html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
