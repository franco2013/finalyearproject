/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author franks
 */
@Embeddable
public class DrilltypePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "DrillTypeID")
    private String drillTypeID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "Drill_ID")
    private String drillID;

    public DrilltypePK() {
    }

    public DrilltypePK(String drillTypeID, String drillID) {
        this.drillTypeID = drillTypeID;
        this.drillID = drillID;
    }

    public String getDrillTypeID() {
        return drillTypeID;
    }

    public void setDrillTypeID(String drillTypeID) {
        this.drillTypeID = drillTypeID;
    }

    public String getDrillID() {
        return drillID;
    }

    public void setDrillID(String drillID) {
        this.drillID = drillID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drillTypeID != null ? drillTypeID.hashCode() : 0);
        hash += (drillID != null ? drillID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DrilltypePK)) {
            return false;
        }
        DrilltypePK other = (DrilltypePK) object;
        if ((this.drillTypeID == null && other.drillTypeID != null) || (this.drillTypeID != null && !this.drillTypeID.equals(other.drillTypeID))) {
            return false;
        }
        if ((this.drillID == null && other.drillID != null) || (this.drillID != null && !this.drillID.equals(other.drillID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.DrilltypePK[ drillTypeID=" + drillTypeID + ", drillID=" + drillID + " ]";
    }
    
}
