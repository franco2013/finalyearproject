/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franks
 */
@Entity
@Table(name = "drilltype")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Drilltype.findAll", query = "SELECT d FROM Drilltype d"),
    @NamedQuery(name = "Drilltype.findByDrillTypeID", query = "SELECT d FROM Drilltype d WHERE d.drilltypePK.drillTypeID = :drillTypeID"),
    @NamedQuery(name = "Drilltype.findByDrillID", query = "SELECT d FROM Drilltype d WHERE d.drilltypePK.drillID = :drillID")})
public class Drilltype implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected DrilltypePK drilltypePK;
    @Lob
    @Size(max = 65535)
    @Column(name = "Name")
    private String name;
    @JoinColumn(name = "Drill_ID", referencedColumnName = "Drill_ID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Drill drill;

    public Drilltype() {
    }

    public Drilltype(DrilltypePK drilltypePK) {
        this.drilltypePK = drilltypePK;
    }

    public Drilltype(String drillTypeID, String drillID) {
        this.drilltypePK = new DrilltypePK(drillTypeID, drillID);
    }

    public DrilltypePK getDrilltypePK() {
        return drilltypePK;
    }

    public void setDrilltypePK(DrilltypePK drilltypePK) {
        this.drilltypePK = drilltypePK;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Drill getDrill() {
        return drill;
    }

    public void setDrill(Drill drill) {
        this.drill = drill;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drilltypePK != null ? drilltypePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Drilltype)) {
            return false;
        }
        Drilltype other = (Drilltype) object;
        if ((this.drilltypePK == null && other.drilltypePK != null) || (this.drilltypePK != null && !this.drilltypePK.equals(other.drilltypePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Drilltype[ drilltypePK=" + drilltypePK + " ]";
    }
    
}
