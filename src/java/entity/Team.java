/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author franks
 */
@Entity
@Table(name = "team")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Team.findAll", query = "SELECT t FROM Team t"),
    @NamedQuery(name = "Team.findByTeamID", query = "SELECT t FROM Team t WHERE t.teamID = :teamID"),
    @NamedQuery(name = "Team.findByEstDate", query = "SELECT t FROM Team t WHERE t.estDate = :estDate"),
    @NamedQuery(name = "Team.findByClubClubID", query = "SELECT t FROM Team t WHERE t.clubClubID = :clubClubID")})
public class Team implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "TeamID")
    private Integer teamID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "EstDate")
    private String estDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Club_Club_ID")
    private int clubClubID;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "League_LeagueID")
    private String leagueLeagueID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "teamTeamID")
    private Collection<Session> sessionCollection;
    @JoinColumn(name = "Club_ClubID", referencedColumnName = "ClubID")
    @ManyToOne(optional = false)
    private Club clubClubID1;

    public Team() {
    }

    public Team(Integer teamID) {
        this.teamID = teamID;
    }

    public Team(Integer teamID, String estDate, int clubClubID, String leagueLeagueID) {
        this.teamID = teamID;
        this.estDate = estDate;
        this.clubClubID = clubClubID;
        this.leagueLeagueID = leagueLeagueID;
    }

    public Integer getTeamID() {
        return teamID;
    }

    public void setTeamID(Integer teamID) {
        this.teamID = teamID;
    }

    public String getEstDate() {
        return estDate;
    }

    public void setEstDate(String estDate) {
        this.estDate = estDate;
    }

    public int getClubClubID() {
        return clubClubID;
    }

    public void setClubClubID(int clubClubID) {
        this.clubClubID = clubClubID;
    }

    public String getLeagueLeagueID() {
        return leagueLeagueID;
    }

    public void setLeagueLeagueID(String leagueLeagueID) {
        this.leagueLeagueID = leagueLeagueID;
    }

    @XmlTransient
    public Collection<Session> getSessionCollection() {
        return sessionCollection;
    }

    public void setSessionCollection(Collection<Session> sessionCollection) {
        this.sessionCollection = sessionCollection;
    }

    public Club getClubClubID1() {
        return clubClubID1;
    }

    public void setClubClubID1(Club clubClubID1) {
        this.clubClubID1 = clubClubID1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (teamID != null ? teamID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Team)) {
            return false;
        }
        Team other = (Team) object;
        if ((this.teamID == null && other.teamID != null) || (this.teamID != null && !this.teamID.equals(other.teamID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Team[ teamID=" + teamID + " ]";
    }
    
}
