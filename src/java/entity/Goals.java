/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franks
 */
@Entity
@Table(name = "goals")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Goals.findAll", query = "SELECT g FROM Goals g"),
    @NamedQuery(name = "Goals.findByGoalID", query = "SELECT g FROM Goals g WHERE g.goalID = :goalID"),
    @NamedQuery(name = "Goals.findByNumberOfGoals", query = "SELECT g FROM Goals g WHERE g.numberOfGoals = :numberOfGoals")})
public class Goals implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "GoalID")
    private Integer goalID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "NumberOfGoals")
    private int numberOfGoals;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "Season")
    private String season;
    @JoinColumn(name = "Member_MemberID", referencedColumnName = "MemberID")
    @ManyToOne(optional = false)
    private Member1 memberMemberID;

    public Goals() {
    }

    public Goals(Integer goalID) {
        this.goalID = goalID;
    }

    public Goals(Integer goalID, int numberOfGoals, String season) {
        this.goalID = goalID;
        this.numberOfGoals = numberOfGoals;
        this.season = season;
    }

    public Integer getGoalID() {
        return goalID;
    }

    public void setGoalID(Integer goalID) {
        this.goalID = goalID;
    }

    public int getNumberOfGoals() {
        return numberOfGoals;
    }

    public void setNumberOfGoals(int numberOfGoals) {
        this.numberOfGoals = numberOfGoals;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public Member1 getMemberMemberID() {
        return memberMemberID;
    }

    public void setMemberMemberID(Member1 memberMemberID) {
        this.memberMemberID = memberMemberID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (goalID != null ? goalID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Goals)) {
            return false;
        }
        Goals other = (Goals) object;
        if ((this.goalID == null && other.goalID != null) || (this.goalID != null && !this.goalID.equals(other.goalID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Goals[ goalID=" + goalID + " ]";
    }
    
}
