/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author franks
 */
@Entity
@Table(name = "session")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Session.findAll", query = "SELECT s FROM Session s"),
    @NamedQuery(name = "Session.findBySessionID", query = "SELECT s FROM Session s WHERE s.sessionID = :sessionID"),
    @NamedQuery(name = "Session.findByDate", query = "SELECT s FROM Session s WHERE s.date = :date"),
    @NamedQuery(name = "Session.findByStartTime", query = "SELECT s FROM Session s WHERE s.startTime = :startTime"),
    @NamedQuery(name = "Session.findByEndTime", query = "SELECT s FROM Session s WHERE s.endTime = :endTime")})
public class Session implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "SessionID")
    private Integer sessionID;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Date")
    @Temporal(TemporalType.DATE)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Column(name = "StartTime")
    @Temporal(TemporalType.TIME)
    private Date startTime;
    @Basic(optional = false)
    @NotNull
    @Column(name = "EndTime")
    @Temporal(TemporalType.TIME)
    private Date endTime;
    @JoinColumn(name = "Team_TeamID", referencedColumnName = "TeamID")
    @ManyToOne(optional = false)
    private Team teamTeamID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "session")
    private Collection<Sessiontype> sessiontypeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sessionSessionID")
    private Collection<Drill> drillCollection;

    public Session() {
    }

    public Session(Integer sessionID) {
        this.sessionID = sessionID;
    }

    public Session(Integer sessionID, Date date, Date startTime, Date endTime) {
        this.sessionID = sessionID;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public Integer getSessionID() {
        return sessionID;
    }

    public void setSessionID(Integer sessionID) {
        this.sessionID = sessionID;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Team getTeamTeamID() {
        return teamTeamID;
    }

    public void setTeamTeamID(Team teamTeamID) {
        this.teamTeamID = teamTeamID;
    }

    @XmlTransient
    public Collection<Sessiontype> getSessiontypeCollection() {
        return sessiontypeCollection;
    }

    public void setSessiontypeCollection(Collection<Sessiontype> sessiontypeCollection) {
        this.sessiontypeCollection = sessiontypeCollection;
    }

    @XmlTransient
    public Collection<Drill> getDrillCollection() {
        return drillCollection;
    }

    public void setDrillCollection(Collection<Drill> drillCollection) {
        this.drillCollection = drillCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sessionID != null ? sessionID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Session)) {
            return false;
        }
        Session other = (Session) object;
        if ((this.sessionID == null && other.sessionID != null) || (this.sessionID != null && !this.sessionID.equals(other.sessionID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Session[ sessionID=" + sessionID + " ]";
    }
    
}
