/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author franks
 */
@Embeddable
public class SessiontypePK implements Serializable {
    @Basic(optional = false)
    @NotNull
    @Column(name = "Name")
    private int name;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Session_SessionID")
    private int sessionSessionID;

    public SessiontypePK() {
    }

    public SessiontypePK(int name, int sessionSessionID) {
        this.name = name;
        this.sessionSessionID = sessionSessionID;
    }

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    public int getSessionSessionID() {
        return sessionSessionID;
    }

    public void setSessionSessionID(int sessionSessionID) {
        this.sessionSessionID = sessionSessionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) name;
        hash += (int) sessionSessionID;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SessiontypePK)) {
            return false;
        }
        SessiontypePK other = (SessiontypePK) object;
        if (this.name != other.name) {
            return false;
        }
        if (this.sessionSessionID != other.sessionSessionID) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.SessiontypePK[ name=" + name + ", sessionSessionID=" + sessionSessionID + " ]";
    }
    
}
