/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franks
 */
@Entity
@Table(name = "playerstats")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Playerstats.findAll", query = "SELECT p FROM Playerstats p"),
    @NamedQuery(name = "Playerstats.findByMemberID", query = "SELECT p FROM Playerstats p WHERE p.memberID = :memberID"),
    @NamedQuery(name = "Playerstats.findByName", query = "SELECT p FROM Playerstats p WHERE p.name = :name"),
    @NamedQuery(name = "Playerstats.findByPhoneNo", query = "SELECT p FROM Playerstats p WHERE p.phoneNo = :phoneNo"),
    @NamedQuery(name = "Playerstats.findByHeight", query = "SELECT p FROM Playerstats p WHERE p.height = :height"),
    @NamedQuery(name = "Playerstats.findByPosition", query = "SELECT p FROM Playerstats p WHERE p.position = :position"),
    @NamedQuery(name = "Playerstats.findByGoalsScored", query = "SELECT p FROM Playerstats p WHERE p.goalsScored = :goalsScored"),
    @NamedQuery(name = "Playerstats.findByStartSeason", query = "SELECT p FROM Playerstats p WHERE p.startSeason = :startSeason"),
    @NamedQuery(name = "Playerstats.findByFoot", query = "SELECT p FROM Playerstats p WHERE p.foot = :foot")})
public class Playerstats implements Serializable {
    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "MemberID")
    private String memberID;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "Name")
    private String name;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "Phone No")
    private Integer phoneNo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "Height")
    private String height;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "Position")
    private String position;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "Goals Scored")
    private String goalsScored;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "Start Season")
    private String startSeason;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 11)
    @Column(name = "Foot")
    private String foot;

    public Playerstats() {
    }

    public Playerstats(Integer phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Playerstats(Integer phoneNo, String memberID, String name, String height, String position, String goalsScored, String startSeason, String foot) {
        this.phoneNo = phoneNo;
        this.memberID = memberID;
        this.name = name;
        this.height = height;
        this.position = position;
        this.goalsScored = goalsScored;
        this.startSeason = startSeason;
        this.foot = foot;
    }

    public String getMemberID() {
        return memberID;
    }

    public void setMemberID(String memberID) {
        this.memberID = memberID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(Integer phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getGoalsScored() {
        return goalsScored;
    }

    public void setGoalsScored(String goalsScored) {
        this.goalsScored = goalsScored;
    }

    public String getStartSeason() {
        return startSeason;
    }

    public void setStartSeason(String startSeason) {
        this.startSeason = startSeason;
    }

    public String getFoot() {
        return foot;
    }

    public void setFoot(String foot) {
        this.foot = foot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (phoneNo != null ? phoneNo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Playerstats)) {
            return false;
        }
        Playerstats other = (Playerstats) object;
        if ((this.phoneNo == null && other.phoneNo != null) || (this.phoneNo != null && !this.phoneNo.equals(other.phoneNo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Playerstats[ phoneNo=" + phoneNo + " ]";
    }
    
}
