/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author franks
 */
@Entity
@Table(name = "sessiontype")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sessiontype.findAll", query = "SELECT s FROM Sessiontype s"),
    @NamedQuery(name = "Sessiontype.findByName", query = "SELECT s FROM Sessiontype s WHERE s.sessiontypePK.name = :name"),
    @NamedQuery(name = "Sessiontype.findBySessionSessionID", query = "SELECT s FROM Sessiontype s WHERE s.sessiontypePK.sessionSessionID = :sessionSessionID")})
public class Sessiontype implements Serializable {
    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SessiontypePK sessiontypePK;
    @Lob
    @Size(max = 65535)
    @Column(name = "Description")
    private String description;
    @JoinColumn(name = "Session_SessionID", referencedColumnName = "SessionID", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Session session;

    public Sessiontype() {
    }

    public Sessiontype(SessiontypePK sessiontypePK) {
        this.sessiontypePK = sessiontypePK;
    }

    public Sessiontype(int name, int sessionSessionID) {
        this.sessiontypePK = new SessiontypePK(name, sessionSessionID);
    }

    public SessiontypePK getSessiontypePK() {
        return sessiontypePK;
    }

    public void setSessiontypePK(SessiontypePK sessiontypePK) {
        this.sessiontypePK = sessiontypePK;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sessiontypePK != null ? sessiontypePK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sessiontype)) {
            return false;
        }
        Sessiontype other = (Sessiontype) object;
        if ((this.sessiontypePK == null && other.sessiontypePK != null) || (this.sessiontypePK != null && !this.sessiontypePK.equals(other.sessiontypePK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Sessiontype[ sessiontypePK=" + sessiontypePK + " ]";
    }
    
}
