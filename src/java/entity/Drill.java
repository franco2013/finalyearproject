/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author franks
 */
@Entity
@Table(name = "drill")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Drill.findAll", query = "SELECT d FROM Drill d"),
    @NamedQuery(name = "Drill.findByDrillID", query = "SELECT d FROM Drill d WHERE d.drillID = :drillID")})
public class Drill implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "Drill_ID")
    private String drillID;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "drill")
    private Collection<Drilltype> drilltypeCollection;
    @JoinColumn(name = "Session_SessionID", referencedColumnName = "SessionID")
    @ManyToOne(optional = false)
    private Session sessionSessionID;

    public Drill() {
    }

    public Drill(String drillID) {
        this.drillID = drillID;
    }

    public String getDrillID() {
        return drillID;
    }

    public void setDrillID(String drillID) {
        this.drillID = drillID;
    }

    @XmlTransient
    public Collection<Drilltype> getDrilltypeCollection() {
        return drilltypeCollection;
    }

    public void setDrilltypeCollection(Collection<Drilltype> drilltypeCollection) {
        this.drilltypeCollection = drilltypeCollection;
    }

    public Session getSessionSessionID() {
        return sessionSessionID;
    }

    public void setSessionSessionID(Session sessionSessionID) {
        this.sessionSessionID = sessionSessionID;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (drillID != null ? drillID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Drill)) {
            return false;
        }
        Drill other = (Drill) object;
        if ((this.drillID == null && other.drillID != null) || (this.drillID != null && !this.drillID.equals(other.drillID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Drill[ drillID=" + drillID + " ]";
    }
    
}
