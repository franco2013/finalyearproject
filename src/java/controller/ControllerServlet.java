/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;
import entity.Admin;
import entity.Club;
import entity.Member1;
import entity.Playerstats;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import session.AdminFacade;
import session.ClubFacade;
import session.Member1Facade;
import session.PlayerstatsFacade;


@WebServlet(name="ControllerServlet",loadOnStartup = 1,urlPatterns = {"/regPage","/createPlayerStats","/logOnPage","/playerStatsPage","/regPageClub","/regPagePerson","/admin","/clubTableMod","clubTableDelete","clubTableUpdate","/playerS","/playerTableDelete","/playerTableUpdate","/adminLogOnPage","/theLogOnPage","/theRegClubDetailsPage","/thePlayerStatsPage","/theIndexPage","/theDisplayMembersPage"})
public class ControllerServlet extends HttpServlet { 
    
      @EJB
      private PlayerstatsFacade anyPlayerStatsFacade;        
      @EJB      
      private ClubFacade anyClubFacade;
      
      @EJB
      private Member1Facade anyMemberFacade;  
      @EJB
      private Member1Facade Member;        // this will be used to get password and username from database
      @EJB
      private AdminFacade admin;           // this will be to ckeck or admin login
      
      private boolean validateAdmin;
      
  @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException
    {
        
         
         String userPath = request.getServletPath();
            if (userPath.equals("/regPagePerson")) 
             {
                 // take in data from form 
                 //from RegPage.jsp
                 
            String clubID = request.getParameter("clubID");
            
            String name = request.getParameter("name");
            String userName = request.getParameter("username");
            
      
            int phoneNum = Integer.parseInt(request.getParameter("phoneNo"));
            String password = request.getParameter("password");
            
            String email = request.getParameter("email");  
            String address = request.getParameter("address");
                                                                                          //setting the attributes so we 
                                               
       Club theClub = anyClubFacade.find(clubID);          //calling the find method to get corrasponding clubid                                                       
            
       Member1 anyMenber = new Member1(name,name,email,phoneNum,password,userName,"image");  
       
       anyMenber.setClubClubID(theClub);                  //set the forign key in the member object
                  
       anyMemberFacade.create(anyMenber);                 //calling the create method in the memberFacade pesisit to database 
       
         // after persisting to the database sucessfully we redirect to the welcome Page
        
       request.setAttribute("username",userName);          //passing values to the welcome page
       
       request.setAttribute("email",email);
       
       RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/view/welcomePage.jsp");   
            
       anyDispatcher.forward(request,response);
       
                                                                                           
      }
            else if (userPath.equals("/playerStatsPage")) 
      {                                   // here we will persist data from the 
                                          //  PlayerStats Page to the database
                                          // assigning form data to scoped varibles            
                    
            String name = request.getParameter("name");
            
                         
            int anyPhoneNo = Integer.parseInt( request.getParameter("phoneNo") );
            
            String height = request.getParameter("height");
             
            String position = request.getParameter("position");           
            
            String goalScored = request.getParameter("goals"); 
            
            String season = request.getParameter("season");     
            
            String foot = request.getParameter("foot"); 
            
            String member = request.getParameter("member");
           
            Playerstats thePlayerStat = new Playerstats(anyPhoneNo,member,name,height, position,goalScored,season,foot);
             
            anyPlayerStatsFacade.create(thePlayerStat);          //persist to database
            
             
            
            
            
       /*   RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WelcomePage.jsp");
           dispatcher.forward(request,response);*/
        
        } 
        if (userPath.equals("/regPageClub"))
        {
            String ClubID = request.getParameter("clubID");  
            
            String Name = request.getParameter("name");
            
            String Address = request.getParameter("address");
            
            int anyPhoneNo = Integer.parseInt(request.getParameter("phoneNo"));       
            
            Club theClub = new Club(ClubID, anyPhoneNo, Address, Name);
            
            anyClubFacade.create(theClub);             
                                                                                   //controll now passed to the 
         request.setAttribute("clubID", ClubID); 
                                                                                   // secondPart of the regitration process
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/view/RegPersonelDetails.jsp");   
         anyDispatcher.forward(request, response);  
        
        }else if (userPath.equals("/adminLogOnPage")){     //from Application Welcome Page  start of admin           
            
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/admin/adminLoginPage.jsp");   
            
         anyDispatcher.forward(request,response);  
            
            
        }else if(userPath.equals("/clubTableMod"))                                 //start of Admin access
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/admin/clubTableMod.jsp");   
         anyDispatcher.forward(request,response);      
            
        }else if(userPath.equals("/clubTableDelete"))
        {
            
           String ClubID = request.getParameter("clubid"); 
           
           List<Member1> memberList = Member.findAll();          //member list now has a list of all the members          
                                                                 //now we search thru looking for the records or entites that have a particular clubid
            
           for (Member1 entity : memberList)                     //enhanced for loop to search thru 
           {                                                     //the member Table with clubid's
              if(entity.getClubClubID().getClubID().equalsIgnoreCase(ClubID))     
              {
                  anyMemberFacade.remove(entity);             
                  
              }// end if statment
            
           }//end for loop
           
       
                    
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/admin/clubTableDelete.jsp");   
         anyDispatcher.forward(request, response);    
            
            
        }else if(userPath.equals("/clubTableUpdate"))
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/admin/clubTableUpdate.jsp");   
         anyDispatcher.forward(request,response);            
            
        } else if(userPath.equals("/playerS"))
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/admin/playerS.jsp");   
          anyDispatcher.forward(request,response);            
            
        }else if(userPath.equals("/playerTableUpdate"))
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/admin/playerSUpdate.jsp");   
         anyDispatcher.forward(request,response);           
            
        } else if(userPath.equals("/playerTableDelete"))
        {
                                                                          //take in the clubid here
         
            
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/admin/playerSDelete.jsp");   
         anyDispatcher.forward(request,response);             
            
        } else if(userPath.equals("/theLogOnPage"))
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/LogOnPage.jsp");   
         anyDispatcher.forward(request,response);             
            
        } else if(userPath.equals("/theRegClubDetailsPage"))          //this is a call from logOnFailurePage
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/view/RegClubDetails.jsp");   
         anyDispatcher.forward(request,response);             
            
        } else if(userPath.equals("/thePlayerStatsPage"))          //this is a call from any page
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/view/displayPlayerStats.jsp");   
         anyDispatcher.forward(request,response);             
            
        } else if(userPath.equals("/theIndexPage"))          //this is a call from any page
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/index.jsp");   
         anyDispatcher.forward(request,response);             
            
        } else if(userPath.equals("/theDisplayMembersPage"))          //this is a call from any page
        {
            
         RequestDispatcher anyDispatcher = request.getServletContext().getRequestDispatcher("/WEB-INF/view/displayMembers.jsp");   
         anyDispatcher.forward(request,response);             
            
        } 

    }// end doGet Method        
  
     
   @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        PrintWriter out = response.getWriter();
        boolean isHere = false;
        String userPath = request.getServletPath();

          // we will use the do post to other opearations
         // taking username and password from the logon form
         // see if exists in the database
         //if it is there direct to welcom page
         //if not there direct to regPage
        
        if (userPath.equals("/logOnPage"))
        {          
                                              
          String username = request.getParameter("username");
          
          String email = request.getParameter("email");
            
          
          //now search the database for the username and password 
          //we will look in the member table
          //List<T> findAll() {
          
           List<Member1> memberList = Member.findAll();       //member list now has a list of all the members          
                                                             //now we search thru looking for the password in each entity
          
           
           
           
           for (Member1 entity : memberList)                 //enhanced for loop to search thru 
           {                                                 //the database of existing usernames
             if(entity.getUserName().equalsIgnoreCase(username)){                   
                                                               //we have serched thru and found it exists
                                                            //so now we redirect to login succsess / welcome page
                                                            //create session varible on sucess of looging in
                 isHere = true;
                    
                 if(isHere == true) 
                 {
                     
                 request.setAttribute("username", username); //setting attributes for welcom page                 
                 
                 request.setAttribute("email", email);       //setting the email attribute 
                 
                 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/welcomePage.jsp");
                 
                 dispatcher.forward(request,response);  
                 }//end if statment
               }
             
             }// end for loop 
                 
               
              //here we have established that it 
              //dosen't exist in the database we redirect back to the logon page
              //System.out.print("Not here...");
              RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/loginFailure.jsp");
                   
              dispatcher.forward(request,response);                                           
                                                
                                                
        }         
   
         else if (userPath.equals("/regPageZ")) 
        {                                             //TEMP CLUBS DATA
            
          
            
            String ClubID = request.getParameter("clubID");    
            
            int anyPhoneNo = Integer.parseInt(request.getParameter("phoneNo"));
            
            String Address = request.getParameter("address");
            String Name = request.getParameter("name");
            
            Club theClub = new Club(ClubID, anyPhoneNo, Address, Name);
            
            anyClubFacade.create(theClub);          

        
        } else if (userPath.equals("/admin"))  //from admin logOn Page----------from here down is admin
        {
             
             String adminName = request.getParameter("adminName");
          
             String password = request.getParameter("password");
             
             List<Admin> adminList = admin.findAll();  
             
             for (Admin entity : adminList)                  //enhanced for loop to search thru 
           {                                                 //the database of existing usernames
             if(entity.getAdminName().equalsIgnoreCase(adminName) && entity.getPassword().equalsIgnoreCase(password) ){                   
                                                                                                                  //we have serched thru and found it exists
                                                                                                                  //so now we redirect to login succsess / welcome page
                //create session varible on sucess of looging in
                 validateAdmin = true;
                    
                 if(validateAdmin == true) 
                 {
                     
                 request.setAttribute("adminName", adminName);     //setting attributes for welcome page                 
                 
                 request.setAttribute("password", password);       //setting the password attribute 
                 
                 RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/admin/adminWelcomePage.jsp");
                 
                 dispatcher.forward(request,response);  
                 } //end if statement
               
             
             }// end for loop 
             
              RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/WEB-INF/view/adminFailurePage.jsp");                     
              dispatcher.forward(request,response);      
              }         
        }       
       
    }//end doPost Method  
}//end ContollereServlet

                                                                                                                /**
                                                                                                                * Returns a short description of the servlet.
                                                                                                                *
                                                                                                                * @return a String containing servlet description

                                                                                                                @Override
                                                                                                                public String getServletInfo() {
                                                                                                                    return "Short description";
                                                                                                                }// </editor-fold>
                                                                                                            }*/
  
                    
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
      
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
       