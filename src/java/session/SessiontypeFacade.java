/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package session;

import entity.Sessiontype;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author franks
 */
@Stateless
public class SessiontypeFacade extends AbstractFacade<Sessiontype> {
    @PersistenceContext(unitName = "projectPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SessiontypeFacade() {
        super(Sessiontype.class);
    }
    
}
