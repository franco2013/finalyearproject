package mail;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class sendMail extends HttpServlet {
@Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
         
          


   /*Retrieve value from the text field using getParameter() 
   method on Request object. Otherwise you can set it directly 
      also if you are not using any interface */  

         final  String to=request.getParameter("to");
         final  String subject=request.getParameter("sub");
         final  String msg=request.getParameter("msg");
         final String user=request.getParameter("eid");
         final String pass=request.getParameter("pass");

     //create an instance of Properties Class     
     Properties props = new Properties();
     
     /* Specifies the IP address of your default mail server
  for e.g if you are using gmail server as an email sever
  you will pass smtp.gmail.com as value of mail.smtp host. As 
  shown here in the coding. Change accordingly, if your email id is not an gmail id*/


     props.put("mail.smtp.host", "smtp.gmail.com");
    // props.put("mail.smtp.port", "587"); //this is optional
     props.put("mail.smtp.auth", "true");
     props.put("mail.smtp.starttls.enable", "true");

     

     /*Pass Properties object(props) and Authenticator object   
           for authentication to Session instance */
     
    Session session = Session.getInstance(props,
                        new javax.mail.Authenticator() {
                           
                @Override
  protected PasswordAuthentication getPasswordAuthentication() {
   return new PasswordAuthentication(user,pass);
   }
});
   
   /* Create an instance of MimeMessage, it accept MIME types and headers */ 
    
 MimeMessage message = new MimeMessage(session);
 message.setFrom(new InternetAddress(user));
 message.addRecipient(Message.RecipientType.TO,new InternetAddress(to));
 message.setSubject(subject);
 message.setText(msg);
 

  /* Transport class is used to deliver the message to the recipients */ 

 Transport.send(message);
 out.println("<div style=' font-size:40px; '>"+"Email Send succesfully.."+"</div>");

        }catch(Exception e){ e.printStackTrace();out.println(e.getMessage());} finally {            
            out.close();
        }
    }

 
}
